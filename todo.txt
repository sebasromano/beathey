Maquetas que proponía hacer:

1) Home -> index.html
2) Listado de imagenes desordenado -> tracklist.html
3) Grillas de 5 columnas desordenadas (alturas diferentes o iguales) -> agenda.html
4) Modales (Popup) -> modal.html
5) Artistas - Listado tipo playlist -> artista.html
6) Artistas - Desarrollo nota -> artista-info.html
7) Artista - Listado eventos / noticias ->  artista-evento.html
8) Shop -> shop-detail.html
9) Noticias -> news.html y news-detail.html
10) Listado de temas / autores -> new-release.html


Grupos:


1) Home

Beathey_HomePage.jpg
Beathey_HomePage_logueado.jpg
Beathey_HomePage_Menu_logueado.jpg

2) Listado de imagenes desordenado

Beathey_Shop.jpg
Beathey_Tracklist_Grilla.jpg

3) Grillas de 5 columnas desordenadas (alturas diferentes o iguales)

Beathey_Booking.jpg
Beathey_Artistas.jpg
Beathey_NextVinylContest.jpg
Beathey_Agenda.jpg
Beathey_Agenda_Calendario.jpg
Beathey_WallBTY.jpg

4) Modales (Popup)

Beathey_Iniciar_Sesion.jpg
Beathey_WallBTY_Comentario.jpg
Beathey_Tracklist_Compartir.jpg
Beathey_WallBTY_Login.jpg
Beathey_Registro.jpg

5) Artistas - Listado tipo playlist

Beathey_Artistas_Desarrollo_NewRelease.jpg
Beathey_Artistas_Desarrollo_SetTrackList.jpg
Beathey_Artistas_Desarrollo_tracks.jpg
Beathey_Booking_Desarrollo_Playlist.jpg
Beathey_Booking_Desarrollo_tracks.jpg
Beathey_Featured_Playlist.jpg

6) Artistas - Desarrollo nota

Beathey_Artistas_Desarrollo_Info.jpg
Beathey_Booking_Desarrollo_Info.jpg
Beathey_Featured_Nota.jpg
Beathey_Noticias_Desarrollo.jpg

7) Artista - Listado eventos / noticias

Beathey_Artistas_Desarrollo_Eventos.jpg
Beathey_Booking_Desarrollo_Eventos.jpg

8) Shop

Beathey_Shop_Desarrollo.jpg

9) Noticias

Beathey_Noticias.jpg

10) Listado de temas / autores

Beathey_Other_Featured_Playlist.jpg
Beathey_Tracklist_Listado.jpg
Beathey_NewRelease.jpg
Beathey_Tracklist.jpg