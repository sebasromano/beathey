var initViewport = function(){
	var viewportHeight = $(window).height();
	$('.min-viewport-h').css('min-height', viewportHeight);
	// bugfix especial para home
	if( $('#home-news').size() > 0)
	{
		var newsHeight = $('#home-news').height();
		var bannerHeight = viewportHeight - newsHeight;
		// banner home
		if( bannerHeight > 340)
		{
			$('.slide-home').height( viewportHeight - newsHeight);
		}
	}
}

var initApp = function(){
	// inicialización
	initViewport();

	// acciones al reiniciar tamaño del browser
	$(window).resize(function() {
		initViewport();
	});

	// Muestra/Oculta el menu principal
	$('.main-nav-toogle').on({
		click: function( e){
			e.preventDefault();
			$('nav').slideToggle();
			$('.user-menu').slideToggle();
		}
	});

	// Muestra/Oculta el menu de usuarios
	$('.user-menu-toggle').on({
		click: function( e){
			e.preventDefault();
			$('.user-menu ul').slideToggle();
		}
	});

	// Masonry Plugin
	if( $('.disordered-columns').size() > 0){
		
		$('.disordered-columns').imagesLoaded( function() {
			var grid = $('.disordered-columns').masonry({gutter: 0, percentPosition: true});
			//grid.masonry('reloadItems');
		});
	}

	// el hover en las listas de temas
	$('.tracklist-item').on({
		mouseover: function(){
			$(this).addClass('tracklist-item-hover');
		},
		mouseout: function(){
			$(this).removeClass('tracklist-item-hover');
		}
	});

	// el hover en las listas de temas
	$('.item-list .item').on({
		mouseover: function(){
			$(this).addClass('item-hover');
		},
		mouseout: function(){
			$(this).removeClass('item-hover');
		}
	});

	if( $('.tabs').size() > 0 ){
		$('.tabs-menu a').on({
			click: function( e){
				var _wrapper = $(this).parents('.tabs:first');
				var _target = $(this).attr('rel');
				e.preventDefault();
				$('.active', _wrapper).removeClass('active');
				$(this).addClass('active');
				$('#' + _target).addClass('active');
			}
		});
	}


	// Calendario
	if( typeof Pikaday != 'undefined')
	{
		var picker = new Pikaday({
                field: document.getElementById('show-calendar'),
                firstDay: 1,
                minDate: new Date(),
                maxDate: new Date(2020, 12, 31),
                yearRange: [2000,2020],
                i18n: {
				    previousMonth : 'Siguiente mes',
				    nextMonth     : 'Mes anterior',
				    months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
				    weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
				    weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
				}
            });
	}

	// Circulo con votación
	if( $('.circle').size() > 0)
	{
		$('.circle').circleProgress( {fill: { color: "#7F7E83" }, thickness: 6, animationStartValue: 1, size: 80 });
	}

	// modal
	if( typeof $.modal === 'function')
	{
		// esto se debe llamar desde un onClick
		$('#modal').modal();		
		$('.modal-share').modal();		

		$('.modalToogle').on({
			click: function( e){
				e.preventDefault();
				$('#' + $(this).attr('rel') ).modal();
			}
		})
	}

	// Switch de contenido
	$('.contentToggle').on({
		click: function( e){
			e.preventDefault();
			$('#' + $(this).attr('rel') ).toggleClass('active');
		}
	});

}

$(function(){
	initApp();
});
